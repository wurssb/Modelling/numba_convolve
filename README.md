# Numba Convolve
Numba version of scipy.ndimage.convolve.

All modes are implemented, but have to be passed using MODE_* constants instead of strings.
Compatible with numba parallelization.

Note: Only implemented for stencils smaller then the array undergoing the convolution.

Testing shows a small speed up for the numba version, which is larger when interfaced with other numba code.

## Requirements:
- `python 3`
- `numba`
- Testing only: `numpy, scipy, pytest`

## Installation:
- The easiest way to install `numba_convolve` is via pip:
  `pip install git+https://gitlab.com/wurssb/Modelling/numba_convolve.git#egg=numba_convolve`

## Authors:
- Rik van Rosmalen

## License:
This project is licensed under the MIT License - see the LICENSE file for details.
