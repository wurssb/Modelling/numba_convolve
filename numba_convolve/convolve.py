"""Numba version of scipy.ndimage.convolve.

All modes are implemented, but have to be passed using MODE_* constants instead of strings.
Compatible with numba parallelization.

Note: Only implemented for stencils smaller then the array undergoing the convolution.
Note: Origin offset is not implemented.

Testing shows a small speed up for the numba version, which is larger when interfaced
with other numba code.

Author: Rik van Rosmalen (rikpetervanrosmalen@gmail.com)
"""

import numba

MODE_constant = 1
MODE_reflect = 2
MODE_nearest = 3
MODE_mirror = 4
MODE_wrap = 5


@numba.njit(
    # numba.int64(numba.int64, numba.int64, numba.int64),
    error_model="numpy",
    fastmath=True,
)
def wrap(i, ma, mode):
    """Wrap index i around 0/max using the specified mode of scipy's convolve.

    :param i: Requested index, that is outside of 0 - ma
    :type inp: int
    :param i: Maximum index. The minimum is assumed to be 0.
    :type inp: mode
    :param mode: Wrapping mode, defaults to MODE_constant.
        Use one of the MODE_* constants.
    :type mode: int, optional
    """
    # ‘reflect’ (d c b a | a b c d | d c b a)
    # The input is extended by reflecting about the edge of the last pixel.
    if mode == MODE_reflect:
        # offset = i - (ma + 1)
        # return ma - offset
        if i > ma:
            return 2 * ma + 1 - i
        else:
            return (-1 * i) - 1
    # ‘nearest’ (a a a a | a b c d | d d d d)
    # The input is extended by replicating the last pixel.
    elif mode == MODE_nearest:
        if i > ma:
            return ma
        else:
            return 0
    # ‘mirror’ (d c b | a b c d | c b a)
    # The input is extended by reflecting about the centre of the last pixel.
    elif mode == MODE_mirror:
        # offset = i - (ma)
        # return ma - offset
        if i > ma:
            return 2 * ma - i
        else:
            return -1 * i
    # ‘wrap’ (a b c d | a b c d | a b c d)
    # The input is extended by wrapping around to the opposite edge.
    elif mode == MODE_wrap:
        if i > ma:
            return (i % ma) - 1
        else:
            return i + ma + 1
    else:
        raise ValueError("Undefined mode.")


@numba.njit(
    # numba.float64[:, :](
    #     numba.float64[:, :],
    #     numba.float64[:, :],
    #     numba.float64[:, :]
    #     numba.int64,
    #     numba.float64,
    # ),
    parallel=True,
    error_model="numpy",
    fastmath=True,
)
def convolve(inp, stencil, outp, mode=MODE_constant, cval=0.0):
    """Multidimensional convolution.

    The array is convolved with the given kernel.
    Numba version of scipy.ndimage.convolve

    :param inp: Input array
    :type inp: np.float[:, :]
    :param stencil: Stencil array
    :type stencil: np.float[:, :]
    :param outp: Output array (same size as input).
    :type outp: np.float[:, :]
    :param mode: Wrapping mode, defaults to MODE_constant.
        Use one of the MODE_* constants.
    :type mode: int, optional
    :param cval: fill value past edges when using MODE_constant, defaults to 0.0
    :type cval: float, optional
    """
    x, y = inp.shape
    s_x, s_y = stencil.shape

    # Check requirements.
    if not s_x % 2 or not s_y % 2:
        raise ValueError("Stencil should have odd dimensions.")
    if s_x > x or s_y > y:
        raise ValueError("Stencils bigger then the convolution target are not implemented.")
    if inp.shape != outp.shape:
        raise ValueError("Input and output should have the same size.")

    # Constants for iterators
    max_x, max_y = x - 1, y - 1
    s_x_start, s_x_half, s_x_end = -(s_x // 2), s_x // 2, (s_x // 2) + 1
    s_y_start, s_y_half, s_y_end = -(s_y // 2), s_y // 2, (s_y // 2) + 1

    # For every point in output[x, y].
    for outp_i in numba.prange(x):
        for outp_j in numba.prange(y):
            v = 0
            # For every point in the stencil[s_x, s_y].
            for s_i in numba.prange(s_x_start, s_x_end):
                for s_j in numba.prange(s_y_start, s_y_end):
                    inp_i = outp_i + s_i
                    inp_j = outp_j + s_j
                    # Correct index for out of bounds cases.
                    if inp_i > max_x or inp_i < 0:
                        if mode == MODE_constant:
                            v += cval * stencil[s_x_half - s_i, s_y_half - s_j]
                            continue
                        else:
                            inp_i = wrap(inp_i, max_x, mode)
                    if inp_j > max_y or inp_j < 0:
                        if mode == MODE_constant:
                            v += cval * stencil[s_x_half - s_i, s_y_half - s_j]
                            continue
                        else:
                            inp_j = wrap(inp_j, max_y, mode)
                    # Update current value
                    # This is s_x_half - s_i, as we start from the negative indices of the stencil.
                    v += stencil[s_x_half - s_i, s_y_half - s_j] * inp[inp_i, inp_j]
            # Finished with this coordinate, so update output.
            outp[outp_i, outp_j] = v
    return outp
