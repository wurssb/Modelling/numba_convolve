"""Tests for convolve.

Author: Rik van Rosmalen (rikpetervanrosmalen@gmail.com)
"""
import numpy as np
import scipy.ndimage
import pytest

import numba_convolve

modes = {
    "reflect": numba_convolve.MODE_reflect,
    "nearest": numba_convolve.MODE_nearest,
    "mirror": numba_convolve.MODE_mirror,
    "wrap": numba_convolve.MODE_wrap,
    "constant": numba_convolve.MODE_constant,
}

all_p = []
all_x = []
all_modes = ["reflect", "nearest", "mirror", "wrap"]
all_cval = [-1, -0.5, 0.0, 0.5, 1]


all_p.append(np.zeros((3, 3)))
all_p.append(np.ones((3, 3)))
all_p.append(np.random.random((5, 5)))
all_p.append(np.random.random((3, 5)))
p = np.ones((3, 3)) * 0.1
p[1, 1] += 0.1
all_p.append(p)

all_x.append(np.zeros((5, 5)))
all_x.append(np.random.random((5, 5)))
all_x.append(np.random.random((6, 5)))
x = np.zeros((5, 5))
x[2, 2] = 10
all_x.append(x)


@pytest.mark.parametrize("test_x", all_x)
@pytest.mark.parametrize("test_p", all_p)
@pytest.mark.parametrize("mode", all_modes)
def test_convolve(test_x, test_p, mode):
    out_scipy = np.zeros(test_x.shape)
    scipy.ndimage.convolve(test_x, test_p, out_scipy, mode)
    out_manual = np.zeros(test_x.shape)
    numba_convolve.convolve(test_x, test_p, out_manual, modes[mode])
    assert out_scipy == pytest.approx(out_manual, rel=1e-6)


@pytest.mark.parametrize("test_x", all_x)
@pytest.mark.parametrize("test_p", all_p)
@pytest.mark.parametrize("cval", all_cval)
def test_convolve_constant(test_x, test_p, cval):
    out_scipy = np.zeros(test_x.shape)
    scipy.ndimage.convolve(test_x, test_p, out_scipy, "constant", cval=cval)
    out_manual = np.zeros(test_x.shape)
    numba_convolve.convolve(test_x, test_p, out_manual, modes["constant"], cval)
    assert out_scipy == pytest.approx(out_manual, rel=1e-6)
