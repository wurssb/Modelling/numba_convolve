"""Numba version of scipy.ndimage.convolve.

Author: Rik van Rosmalen (rikpetervanrosmalen@gmail.com)
"""
import setuptools


def readme():
    """Load readme from README.md."""
    with open("README.md", "r") as fh:
        return fh.read()


setuptools.setup(
    name="numba_convolve",
    version="1.0.0",
    author="Rik van Rosmalen",
    author_email="rikpetervanrosmalen@gmail.com",
    description="Numba version of scipy.ndimage.convolve.",
    long_description=readme(),
    long_description_content_type="text/markdown",
    url="https://gitlab.com/wurssb/Modelling/numba_convolve",
    packages=setuptools.find_packages(),
    classifiers=(
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ),
    install_requires=("numba",),
    extras_require={"tests": ["numpy", "scipy", "pytest"]},
)
